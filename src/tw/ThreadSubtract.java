package tw;

import tw.threadstrategy.ThreadStrategy;

/**
 * Created by Piotr Bochenek on 2015-03-12.
 */
public class ThreadSubtract implements Runnable {


    private final ThreadStrategy strategy;

    public ThreadSubtract(ThreadStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public void run() {
       strategy.subtractLoop();
    }


}

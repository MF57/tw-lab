package tw;

import tw.threadstrategy.ThreadStrategy;

/**
 * Created by Piotr Bochenek on 2015-03-12.
 */
public class ThreadAdd implements Runnable {

    private final ThreadStrategy strategy;


    public ThreadAdd(ThreadStrategy strategy) {
        this.strategy = strategy;
    }


    @Override
    public void run() {
        strategy.addLoop();
    }
}

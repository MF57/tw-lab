package tw;

import tw.threadstrategy.ThreadStrategy;
import tw.threadstrategy.strategies.AtomicStrategy;
import tw.threadstrategy.strategies.DesynchronisedThreadStrategy;
import tw.threadstrategy.strategies.SynchronisedStrategy;

/**
 * Created by Piotr Bochenek on 2015-03-12.
 */
public class Main {

    private Counter counter = new Counter();
    private long currentTime = System.currentTimeMillis();
    private ThreadStrategy strategy;
    private long threadCounter;

    void desynchronisedResourceRace(){
        counter.resetCounter();
        strategy = new DesynchronisedThreadStrategy(counter);
        runThreads();
        System.out.println("Dodałem i odjąłem milion (niesynchronizowane metody): "+counter.getCounter());
        System.out.println("Czas: " + (System.currentTimeMillis()-currentTime));
        currentTime = System.currentTimeMillis();
    }

    void synchronisedResourceRace(){
        counter.resetCounter();
        strategy = new SynchronisedStrategy(counter);
        runThreads();
        System.out.println("Dodałem i odjąłem milion (synchronizowane metody): "+counter.getCounter());
        System.out.println("Czas: " + (System.currentTimeMillis()-currentTime));
        currentTime = System.currentTimeMillis();
    }

    void atomicResourceRace(){
        counter.resetCounter();
        strategy = new AtomicStrategy(counter);
        runThreads();
        System.out.println("Dodałem i odjąłem milion (atomiczne metody): "+counter.getCounter());
        System.out.println("Czas: " + (System.currentTimeMillis()-currentTime));
        currentTime = System.currentTimeMillis();
    }


    void runThreads() {
        Thread t = new Thread(new ThreadAdd(strategy));
        t.start();
        Thread t1 = new Thread(new ThreadSubtract(strategy));
        t1.start();
        try {
            t.join();
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void destroyUniverse(){
        while(true) {
            new Thread(){
                public void run() {
                    try {
                        Thread.sleep(Long.MAX_VALUE);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }.start();
            System.out.println("Created thread number: "+ threadCounter);
            threadCounter++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Main runner = new Main();
        runner.desynchronisedResourceRace();
        runner.synchronisedResourceRace();
        runner.atomicResourceRace();
        runner.destroyUniverse();
    }
}

package tw;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Piotr Bochenek on 2015-03-12.
 */
public class Counter {


    private AtomicInteger atomicCounter = new AtomicInteger(0);
    private int counter = 0;

    public void add() {
        counter++;
    }

    public void subtract() {
        counter--;
    }

    public synchronized void synchronisedAdd(){
        counter++;
    }

    public synchronized void synchronisedSubtract(){
        counter--;
    }

    public void addAtomicCounter() {
        atomicCounter.incrementAndGet();
    }

    public void subtractAtomicCounter() {
        atomicCounter.decrementAndGet();
    }

    public void resetCounter(){
        counter = 0;
        atomicCounter.set(0);
    }

    public AtomicInteger getAtomicCounter() {
        return atomicCounter;
    }

    public int getCounter() {
        return counter;
    }
}

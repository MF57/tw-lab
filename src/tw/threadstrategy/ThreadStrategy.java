package tw.threadstrategy;

/**
 * Created by Piotr Bochenek on 2015-03-13.
 */
public interface ThreadStrategy {

    public void addLoop();
    public void subtractLoop();
}

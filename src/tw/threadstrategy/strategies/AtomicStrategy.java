package tw.threadstrategy.strategies;

import tw.Counter;
import tw.threadstrategy.ThreadStrategy;

/**
 * Created by Piotr Bochenek on 2015-03-13.
 */
public class AtomicStrategy implements ThreadStrategy{

    private final Counter counter;

    public AtomicStrategy(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void addLoop() {
        for(int i = 0; i < 1000000; i++){
            counter.addAtomicCounter();
        }
        System.out.println("Dodałem milion: "+counter.getAtomicCounter());
    }

    @Override
    public void subtractLoop() {
        for(int i = 0; i < 1000000; i++){
            counter.subtractAtomicCounter();
        }
        System.out.println("Odjąłem milion: "+counter.getAtomicCounter());

    }
}

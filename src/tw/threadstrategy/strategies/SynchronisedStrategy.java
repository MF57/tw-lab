package tw.threadstrategy.strategies;

import tw.Counter;
import tw.threadstrategy.ThreadStrategy;

/**
 * Created by Piotr Bochenek on 2015-03-13.
 */
public class SynchronisedStrategy implements ThreadStrategy {

    private final Counter counter;

    public SynchronisedStrategy(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void addLoop() {
        for(int i = 0; i < 1000000; i++){
            counter.synchronisedAdd();
        }
        System.out.println("Dodałem milion: "+counter.getCounter());

    }

    @Override
    public void subtractLoop() {
        for(int i = 0; i < 1000000; i++){
            counter.synchronisedSubtract();
        }
        System.out.println("Odjąłem milion: "+counter.getCounter());

    }
}

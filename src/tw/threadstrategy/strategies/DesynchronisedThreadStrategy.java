package tw.threadstrategy.strategies;

import tw.Counter;
import tw.threadstrategy.ThreadStrategy;

/**
 * Created by Piotr Bochenek on 2015-03-13.
 */
public class DesynchronisedThreadStrategy implements ThreadStrategy {

    private final Counter counter;

    public DesynchronisedThreadStrategy(Counter counter) {
        this.counter = counter;
    }


    @Override
    public void addLoop() {
        for(int i = 0; i < 1000000; i++){
            counter.add();
        }
        System.out.println("Dodałem milion: "+counter.getCounter());
    }

    @Override
    public void subtractLoop() {
        for(int i = 0; i < 1000000; i++){
            counter.subtract();
        }
        System.out.println("Odjąłem milion: "+counter.getCounter());

    }
}

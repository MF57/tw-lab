package tw;

/**
 * Created by Piotr Bochenek on 2015-03-13.
 */
public enum ThreadMode {
    DESYNCHRONISED, SYNCHRONISED, ATOMIC

}
